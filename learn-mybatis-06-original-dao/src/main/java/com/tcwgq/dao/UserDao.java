package com.tcwgq.dao;

import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月13日下午9:58:05
 * @email tcwgq@outlook.com
 */
public interface UserDao {

	public User getById(Integer id) throws Exception;

	public int insertUser(User user) throws Exception;

	public int deleteById(Integer id) throws Exception;
}
