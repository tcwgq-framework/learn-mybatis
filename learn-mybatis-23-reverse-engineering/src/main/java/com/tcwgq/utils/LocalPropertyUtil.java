package com.tcwgq.utils;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;

public class LocalPropertyUtil {

    private static final Logger logger = LoggerFactory.getLogger(LocalPropertyUtil.class);

    private static final String CLASSPATH_URL_PREFIX = "classpath:";

    private static final String configPaths =
        "classpath:application.properties,classpath:config.properties,classpath:important.properties";

    private static PropertiesConfiguration config = new PropertiesConfiguration();

    static {
        String[] configPathArray = configPaths.split(",");
        try {
            for (String configPath : configPathArray) {
                String conf = StringUtils.substringAfter(configPath, CLASSPATH_URL_PREFIX);
                ClassLoader classLoader = LocalPropertyUtil.class.getClassLoader();
                URL resource = classLoader.getResource(conf);
                if (resource != null) {
                    logger.info("init local property utils ,  current config is [{}] , config path is [{}]", configPath,
                        resource.getPath());
                }
                config.load(classLoader.getResourceAsStream(conf));
            }
        } catch (ConfigurationException e) {
            logger.error(" failed to load config from current class path [{}], exception detail is [{}] ", config);
        }
    }

    public static String getString(String key) {
        return config.getString(key);
    }

    public static String getString(String key, String defaultValue) {
        return config.getString(key, defaultValue);
    }

    public static Integer getInteger(String key) {
        return config.getInteger(key, null);
    }

    public static Integer getInteger(String key, Integer defaultValue) {
        return config.getInteger(key, defaultValue);
    }
    
    public static Long getLong(String key) {
        return config.getLong(key, null);
    }

    public static Long getLong(String key, Long defaultValue) {
        return config.getLong(key, defaultValue);
    }

    public static Boolean getBoolean(String key) {
        return config.getBoolean(key, null);
    }

    public static Boolean getBoolean(String key, Boolean defaultValue) {
        return config.getBoolean(key, defaultValue);
    }

    public static String[] getStringArray(String key) {
        return config.getStringArray(key);
    }

    public static <T> List<T> getList(String key, final Class<T> clazz) {
        List<Object> list = config.getList(key);
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        List<T> transform = Lists.transform(list, new Function<Object, T>() {
            @Override
            public T apply(Object input) {
                return JSON.parseObject(JSON.toJSONString(input), clazz);
            }
        });
        return transform;
    }

    public static <T> List<T> getList(String key, List<T> defaultValue, final Class<T> clazz) {
        List<Object> list = config.getList(key);
        if (CollectionUtils.isEmpty(list)) {
            return defaultValue;
        }
        List<T> transform = Lists.transform(list, new Function<Object, T>() {
            @Override
            public T apply(Object input) {
                return JSON.parseObject(JSON.toJSONString(input), clazz);
            }
        });
        return transform;
    }

}
