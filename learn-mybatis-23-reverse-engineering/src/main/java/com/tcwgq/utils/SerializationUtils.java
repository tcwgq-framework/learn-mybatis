package com.tcwgq.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.Serializable;

public class SerializationUtils {

    public static String serialize(Object object) {
        if (null == object) {
            return null;
        }
        byte[] serialize = org.apache.commons.lang3.SerializationUtils.serialize((Serializable)object);
        return Base64.encodeBase64String(serialize);
    }

    public static <T> T deserialize(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        ByteArrayInputStream bis = new ByteArrayInputStream(Base64.decodeBase64(str));
        return org.apache.commons.lang3.SerializationUtils.deserialize(bis);

    }

}
