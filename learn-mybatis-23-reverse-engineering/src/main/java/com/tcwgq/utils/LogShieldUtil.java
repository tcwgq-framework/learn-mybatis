package com.tcwgq.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 防止泄露身份证号和手机号信息
 * User: yfmafei
 * Date: 15-1-21
 * Time: 下午3:57
 * To change this template use File | Settings | File Templates.
 */
public class LogShieldUtil {
    private static final Pattern idno = Pattern.compile("\\d{15}|\\d{18}");

    private static final Pattern mobile = Pattern.compile("\\d{11}");

    public static String shieldWords(String Data) {
        if (Data == null) {
            return "";
        }
        String logs = new String(Data);
        try {
            Matcher id = idno.matcher(logs);

            while (id.find()) {
                String res = id.group();
                String newres = doreplace(res);
                logs = logs.replace(res, newres);
            }
            Matcher m = mobile.matcher(logs);

            while (m.find()) {
                String res = m.group();
                String newres = doreplace(res);
                logs = logs.replace(res, newres);
            }
        } catch (Exception e) {

        }
        return logs;
    }

    public static String doreplace(String ss) {
        if (ss == null) {
            return "";
        }
        String v = new String(ss);
        switch (v.length()) {
            case 18:
                return v.replace(v.substring(6, 14), "********");
            case 15:
                return v.replace(v.substring(6, 14), "********");
            case 11:
                return v.substring(0, 3) + "****" + v.substring(7);
            default:
                return v.replace(v.substring(v.length() / 2 - 2, v.length() / 2 + 2), "****");
        }

    }

    /***
     * 屏蔽电子邮箱地址
     * @param ss
     * @return
     * Q@jd.com-->Q@jd.com
     * Qw@jd.com-->Q*@jd.com
     * QWe@jd.com-->Q**@jd.com
     * QWER@jd.com-->Q***@jd.com
     * QWER_@jd.com-->Q****@jd.com
     * QWER_23243@jd.com-->Q*********@jd.com
     * 233423243@jd.com-->2********@jd.com
     */
    public static String shieldEmail(String ss) {
        if (ss == null) {
            return "";
        }
        String v = new String(ss);
        String[] emailArr = v.split("@");
        if (emailArr.length == 2) {
            int len = emailArr[0].length();
            String namehead = emailArr[0].substring(0, 1);
//           String nametail = emailArr[0].substring(len-2);
            String shieldName = namehead + getShieldWords(len - 1);
            v = shieldName + "@" + emailArr[1];
        }

        return v;

    }

    /***
     * 屏蔽身份证号
     * @param ss
     * @return
     *
     */
    public static String shieldPaperNumber(String ss) {
        if (ss == null) {
            return "";
        }
        String v = new String(ss);

        if (v.length() > 4) {
            int len = v.length();
            String namehead = v.substring(0, 3);
            String nametail = v.substring(len - 1);
            v = namehead + getShieldWords(len - 4) + nametail;

        }

        return v;

    }

    /***
     * 屏蔽手机号
     * @param ss
     * @return
     *
     */
    public static String shieldMobile(String ss) {
        if (ss == null) {
            return "";
        }
        String v = new String(ss);

        if (v.length() == 11) {
            return v.substring(0, 3) + "****" + v.substring(7);
        }

        return v;

    }

    /***
     * 获取指定长度的屏蔽串
     * @param len
     * @return
     */
    private static String getShieldWords(int len) {
        StringBuffer bf = new StringBuffer();
        for (int i = 0; i < len; i++) {
            bf.append("*");
        }
        return bf.toString();
    }

    /***
     * 是否是被屏蔽的串
     * @param mobile
     * @return
     */
    public static boolean isShieldMobile(String mobile) {
        return mobile.contains("*");
    }

    /**
     * 屏蔽姓名，只显示第一个字符，其他显示为*
     *
     * @param name
     * @return
     */
    public static String shieldName(String name) {
        if (StringUtils.isBlank(name)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.length() - 1; i++) {
            sb.append("*");
        }
        return name.substring(0, 1) + sb.toString();
    }

}
