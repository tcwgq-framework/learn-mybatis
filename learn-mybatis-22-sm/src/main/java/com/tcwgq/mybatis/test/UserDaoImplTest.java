package com.tcwgq.mybatis.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.dao.UserDao;
import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月23日上午9:08:03
 * @email tcwgq@outlook.com
 */
public class UserDaoImplTest {

	@Test
	public void testGetById() throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		UserDao userDao = (UserDao) ctx.getBean("userDao");
		User user = userDao.getById(10);
		System.out.println(user);
	}

}
