package com.tcwgq.mybatis.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.mapper.UserMapper;
import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月15日下午10:05:50
 * @email tcwgq@outlook.com
 */
public class UserMapperTest {
	@Test
	public void testGetById() throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		UserMapper userMapper = (UserMapper) ctx.getBean("userMapper");
		User user = userMapper.getById(10);
		System.out.println(user);
	}

}
