package com.tcwgq.dao.impl;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import com.tcwgq.dao.UserDao;
import com.tcwgq.model.User;

/**
 * 原始dao方式整合spring
 * 
 * @author tcwgq
 * @time 2017年9月13日下午10:00:09
 * @email tcwgq@outlook.com
 */
public class UserDaoImpl extends SqlSessionDaoSupport implements UserDao {

	@Override
	public User getById(Integer id) throws Exception {
		SqlSession session = this.getSqlSession();
		User user = session.selectOne("user.getById", id);
		// 交给spring管理，不需要手动关闭
		// session.close();
		return user;
	}

}
