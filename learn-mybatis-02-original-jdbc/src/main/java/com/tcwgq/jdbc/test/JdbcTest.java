package com.tcwgq.jdbc.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

/**
 * @author tcwgq
 * @time 2017年9月9日下午10:10:01
 * @email tcwgq@outlook.com
 */
public class JdbcTest {
	@Test
	public void testJdbc() {
		String className = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/learn-mybatis";
		String username = "root";
		String password = "112113";
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			Class.forName(className);
			conn = DriverManager.getConnection(url, username, password);
			String sql = "select * from user where username = ?";
			st = conn.prepareStatement(sql);
			st.setString(1, "张三");
			rs = st.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString("id") + "--" + rs.getString("username"));
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
