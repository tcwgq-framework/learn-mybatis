package com.tcwgq.mapper;

import java.util.List;

import com.tcwgq.model.Orders;
import com.tcwgq.model.OrdersCustom;

/**
 * @author tcwgq
 * @time 2017年9月13日下午9:58:05
 * @email tcwgq@outlook.com
 */
public interface OrdersMapper {
	public List<OrdersCustom> findOrdersRusultType() throws Exception;

	public List<Orders> findOrdersRusultMap() throws Exception;
}
