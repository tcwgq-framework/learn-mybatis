package com.tcwgq.mybatis.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.tcwgq.enums.OrderStatus;
import com.tcwgq.mapper.UserMapper;
import com.tcwgq.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class testUser {
	@Autowired
	private UserMapper mapper;

	@Test
	public void testInsert() {
		User user = new User();
		user.setName("zhangSan");
		user.setAge(23);
		user.setEmail("zs@jd.com");
		int row = mapper.insert(user);
		System.out.println(row);
	}

	@Test
	public void testInsertEnum() {
		User user = new User();
		user.setName("liSi");
		user.setAge(24);
		user.setEmail("liSi@jd.com");
		user.setOrderStatus(OrderStatus.PAYED);
		int row = mapper.insert(user);
		System.out.println(row);
	}

	@Test
	public void testInsert1() {
		User user = new User();
		user.setName("wangWu");
		user.setAge(55);
		user.setEmail("wangWu@jd.com");
		user.setOrderStatus(OrderStatus.FINISHED);
		int row = mapper.insert1(user);
		System.out.println(row);
	}

	@Test
	public void testInsert2() {
		User user = new User();
		user.setName("wangWu");
		user.setAge(55);
		user.setEmail("wangWu@jd.com");
		user.setOrderStatus(OrderStatus.FINISHED);
		int row = mapper.insert2(user);
		System.out.println(row);
	}

	@Test
	public void testDelete() {
		Integer row = mapper.deleteById(1l);
		System.out.println(row);
	}

	@Test
	public void testUpdate() {
		User user = new User();
		user.setId(3l);
		user.setName("zhangSan");
		user.setAge(23);
		user.setEmail("zhangSan@jd.com");
		// 使用乐观锁必须带上version字段
		user.setVersion(1);
		int row = mapper.updateById(user);
		System.out.println(row);
	}

	@Test
	public void testUpdate1() {
		User user = new User();
		user.setId(1l);
		user.setName("zhangSan");
		user.setAge(23);
		user.setEmail("zhangSan@jd.com");
		// 使用乐观锁必须带上version字段
		user.setVersion(1);
		int row = mapper.update1(user);
		System.out.println(row);
	}

	@Test
	public void testSelect() {
		User user = mapper.selectById(3);
		System.out.println(JSON.toJSONString(user));
	}

	@Test
	public void testSelect1() {
		User user = mapper.select1(3l);
		System.out.println(JSON.toJSONString(user));
	}

}
