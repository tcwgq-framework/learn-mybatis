package com.tcwgq.mybatis.test;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tcwgq.mapper.UserMapper;
import com.tcwgq.entity.User;
import org.apache.ibatis.session.RowBounds;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class testBaseMapper {
    @Autowired
    private UserMapper mapper;

    @Test
    public void testSelectPage() {
        RowBounds rowBounds = new RowBounds(0, 10);
        User user = new User();
        user.setAge(23);
        user.setName("zhangSan");
        // wrapper
        Wrapper<User> wrapper = new EntityWrapper<>();
        wrapper.like("name", "zhangSan");
        Integer count = mapper.selectCount(wrapper);
        List<User> users = mapper.selectPage(rowBounds, wrapper);
        System.out.println(count);
        System.out.println("==============================");
        System.out.println(JSON.toJSONString(users));
    }


}
