package com.tcwgq.mybatis.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import com.tcwgq.enums.OrderStatus;

@MappedJdbcTypes(JdbcType.TINYINT)
@MappedTypes(OrderStatus.class)
public class OrderStatusTypeHandler extends BaseTypeHandler<OrderStatus> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, OrderStatus parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setInt(i, parameter.getStatus());
	}

	@Override
	public OrderStatus getNullableResult(ResultSet rs, String columnName) throws SQLException {
		int status = rs.getInt(columnName);
		return OrderStatus.getEnum(status);
	}

	@Override
	public OrderStatus getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		int status = rs.getInt(columnIndex);
		return OrderStatus.getEnum(status);
	}

	@Override
	public OrderStatus getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		int status = cs.getInt(columnIndex);
		return OrderStatus.getEnum(status);
	}

}
