package com.tcwgq.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.Version;
import com.baomidou.mybatisplus.enums.FieldFill;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by tcwgq on 2018/10/2 11:26.
 */
public class BaseEntity implements Serializable {
    private Long id;
    @TableLogic
    private Integer deleted;
    @Version
    private Integer version;
    @TableField(value = "time_created", fill = FieldFill.INSERT)
    private Date timeCreated;
    @TableField(value = "time_modified", fill = FieldFill.INSERT_UPDATE)
    private Date timeModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Date getTimeModified() {
        return timeModified;
    }

    public void setTimeModified(Date timeModified) {
        this.timeModified = timeModified;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                ", deleted=" + deleted +
                ", version=" + version +
                ", timeCreated=" + timeCreated +
                ", timeModified=" + timeModified +
                '}';
    }
}
