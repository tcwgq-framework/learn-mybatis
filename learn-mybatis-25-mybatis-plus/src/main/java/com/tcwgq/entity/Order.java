package com.tcwgq.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableName;
import com.tcwgq.entity.BaseEntity;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-02 14:36:55
 */
@TableName("t_order")
public class Order extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private BigDecimal price;

    private String info;


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Order{" +
        "price=" + price +
        ", info=" + info +
        "}";
    }
}
