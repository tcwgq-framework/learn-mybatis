package com.tcwgq.exception;

/**
 * Created by tcwgq on 2018/10/2 15:17.
 */
public class MyRuntimeException extends RuntimeException implements ErrorCode {
    private Integer code;
    private String msg;

    public MyRuntimeException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public MyRuntimeException(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();
    }

    public MyRuntimeException(String message, Integer code, String msg) {
        super(message);
        this.code = code;
        this.msg = msg;
    }

    public MyRuntimeException(String message, Throwable cause, Integer code, String msg) {
        super(message, cause);
        this.code = code;
        this.msg = msg;
    }

    public MyRuntimeException(Throwable cause, Integer code, String msg) {
        super(cause);
        this.code = code;
        this.msg = msg;
    }

    public MyRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Integer code, String msg) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
