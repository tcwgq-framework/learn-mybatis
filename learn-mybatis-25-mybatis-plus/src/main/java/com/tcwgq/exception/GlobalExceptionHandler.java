package com.tcwgq.exception;

import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 通用全局异常处理器，跳转到错误页面
 * Created by tcwgq on 2018/10/2 16:12.
 */
@Component
public class GlobalExceptionHandler implements HandlerExceptionResolver, Ordered {
    public GlobalExceptionHandler() {
        System.out.println("init GlobalExceptionHandler----------------------------------------------------");
    }

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        //if (request.getRequestURI().contains(".action")) {
        //    // 返回null代表不起任何作用
        //    return null;
        //}

        String msg;
        if (ex instanceof BusinessRuntimeException) {
            msg = ((BusinessRuntimeException) ex).getMsg();
        } else if (ex instanceof MyRuntimeException) {
            msg = ((MyRuntimeException) ex).getMsg();
        } else {
            msg = ex.getMessage();
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        // 根据不同的请求后缀，返回不同格式的错误信息，有的跳页面，有的返回json
        if (request.getRequestURI().contains(".action")) {
            MappingJackson2JsonView view = new MappingJackson2JsonView();
            // 这里设置之后，上面的setViewName失效
            modelAndView.setView(view);
        }
        modelAndView.addObject("msg", msg);
        return modelAndView;
    }
}
