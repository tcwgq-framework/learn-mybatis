package com.tcwgq.exception;

/**
 * Created by tcwgq on 2018/10/2 13:55.
 */
public class BusinessRuntimeException extends RuntimeException implements ErrorCode {
    private Integer code;
    private String msg;

    public BusinessRuntimeException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BusinessRuntimeException(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();
    }

    public BusinessRuntimeException(String message, Integer code, String msg) {
        super(message);
        this.code = code;
        this.msg = msg;
    }

    public BusinessRuntimeException(String message, Throwable cause, Integer code, String msg) {
        super(message, cause);
        this.code = code;
        this.msg = msg;
    }

    public BusinessRuntimeException(Throwable cause, Integer code, String msg) {
        super(cause);
        this.code = code;
        this.msg = msg;
    }

    public BusinessRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Integer code, String msg) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
