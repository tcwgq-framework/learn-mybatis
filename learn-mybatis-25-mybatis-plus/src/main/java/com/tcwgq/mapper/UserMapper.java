package com.tcwgq.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcwgq.entity.User;

/**
 * 自己写mapper时，自动填充字段，乐观锁插件，逻辑删除与逻辑不删除这些功能都需要自己实现，并且SQL语句中相关字段一定要写全，MP只有BaseMapper中的方法上述功能会自动实现
 */
public interface UserMapper extends BaseMapper<User> {
	public int insert1(User user);

	public int insert2(User user);

	public User select1(Long id);

	public int update1(User user);

}
