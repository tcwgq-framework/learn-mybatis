package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年9月16日下午6:05:04
 * @email tcwgq@outlook.com
 */
public class UserQueryVo {
	/**
	 * vo: view object 视图对象 po: persistence object 持久层对象
	 */
	// 还可以添加订单，商品等等的额外条件
	private UserCustom userCustom;

	public UserCustom getUserCustom() {
		return userCustom;
	}

	public void setUserCustom(UserCustom userCustom) {
		this.userCustom = userCustom;
	}

	@Override
	public String toString() {
		return "UserQueryVo [userCustom=" + userCustom + "]";
	}

}
